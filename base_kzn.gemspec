$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "base_kzn/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "base_kzn"
  s.version     = BaseKzn::VERSION
  s.authors     = ["Anton Taraev"]
  s.email       = ["ataraev@headmade.pro"]
  s.homepage    = "http://headmade.pro"
  s.summary     = "Base kzn.ru style, guide and js"
  s.description = "Base kzn.ru style, guide and js"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"
  s.add_dependency "haml-rails"
  s.add_dependency "sass-rails"
  s.add_dependency "coffee-rails"
  s.add_dependency "jquery-rails"
  s.add_dependency "rails-assets-svg4everybody"
  s.add_dependency "autoprefixer-rails"
end