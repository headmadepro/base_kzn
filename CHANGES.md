# Change Log

## [1.5.3] - 2015-12-07
## Change element for svg wrapper from `<div>` to `<i>`


## [1.5.0] - 2015-12-07
## Added autoprefixer


## [1.4.0] - 2015-12-07
## Updated foundations SCSS to 6.0.5

### Upgrade from 1.3.0 to 1.4.0
- [ссылка](https://bitbucket.org/headmadepro/base_kzn/diff/lib/generators/base_kzn/templates/stylesheets/base/_foundation_and_overrides.scss?diff2=b7a3a78256ec&at=master)


## [1.3.0] - 2015-12-04

### Добавлено:
  - svg helper
  - svg4everybody
  - стили для svg иконок

### Миграция на версию 1.3.0

-1- Запустить команду `rails generate base_kzn:install`, пропустить все конфликты

-2- В app/assets/javascripts/application.coffee:
  * строчкой ниже `#= require base_kzn` дописать `#= require svg4everybody`
  * строчкой ниже `$(document).foundation()` дописать `svg4everybody()`

-3- В app/assets/stylesheets/base/_foundation_and_overrides.scss:
  * строчкой ниже `// $rem-base: 16px;` дописать `$vertical-gutter: rem-calc(8);`

-4- В файл app/assets/stylesheets/components/_variables.sass дописать:
```
// Icons
// 
$icon-width: $vertical-gutter * 4
$icon-height: $vertical-gutter * 4
$icon-fill: $body-font-color
```

-5- В файл app/assets/stylesheets/components/_icons.sass дописать:
```
.icon-svg
width: 100%
height: 100%

.icon
display: inline-block
width: $icon-width
height: $icon-height
vertical-align: middle
fill: $icon-fill
stroke: none
line-height: 0

@for $i from 1 through 6
.icon-#{$i}x
width: $vertical-gutter * $i
height: $vertical-gutter * $i
```

-6- Если на момент генерации спрайт (app/assets/images/icons_svg.svg переименуйте в icons_svg.svg, если он назывался icons-svg.svg) уже существовал, добавьте в него иконку `icon-default` - используется как заглушка.
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg">
<symbol id="icon-default" viewBox="0 0 32 32">
<circle stroke-width="1" cx="16" cy="16" r="16"/>
</symbol>
</svg>
```

-7- В app/assets/stylesheets/application.sass:
  * строчкой ниже `@import "components/variables"` дописать `@import "components/icons"`