module BaseKzn
  class Engine < ::Rails::Engine
    isolate_namespace BaseKzn

    config.before_initialize do
      ActiveSupport.on_load :action_view do
        include BaseKzn::SvgHelper
      end
    end

  end
end