require "base_kzn/engine"

# Gems
require "haml-rails"
require "sass-rails"
require "coffee-rails"
require "jquery-rails"
require "rails-assets-svg4everybody"
require "autoprefixer-rails"

module BaseKzn
end