module BaseKzn
  module SvgHelper
    def svg_icon(path, params={})

      # defaults
      params = {size: '4'}.merge(params)

      #dirname =   File.dirname(path)
      #extname =   File.extname(path)
      #name =      File.basename(path, extname)


      # Href
      path ||=    "default"
      name =      "icon-#{path}"
      path =      "icons_svg.svg##{name}"
      href =      "#{image_path(path)}"

      # Class
      css_class =  "icon"

      # Icon Name
      css_class += " #{name}"

      # Size
      css_class += " icon-#{params[:size]}x"

      # Color
      if params[:color]
        css_class +=   " icon-color#{params[:color]}"
      end

      if params[:class]
        css_class +=   " #{params[:class]}"
      end

      "<i class='#{css_class}'><svg class='icon-svg'><use xlink:href='#{href}'></use></svg></i>".html_safe

    end
  end
end