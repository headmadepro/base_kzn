#= require base_kzn
#= require svg4everybody

# Foundation
#
#= require base_kzn/foundation/foundation.core
# require base_kzn/foundation/foundation.abide.js
# require base_kzn/foundation/foundation.accordion
# require base_kzn/foundation/foundation.accordionMenu
# require base_kzn/foundation/foundation.drilldown
# require base_kzn/foundation/foundation.dropdown
# require base_kzn/foundation/foundation.dropdownMenu
# require base_kzn/foundation/foundation.equalizer
# require base_kzn/foundation/foundation.interchange
# require base_kzn/foundation/foundation.magellan
# require base_kzn/foundation/foundation.offcanvas
# require base_kzn/foundation/foundation.orbit
# require base_kzn/foundation/foundation.responsiveMenu
# require base_kzn/foundation/foundation.responsiveToggle
# require base_kzn/foundation/foundation.reveal
# require base_kzn/foundation/foundation.slider
# require base_kzn/foundation/foundation.sticky
# require base_kzn/foundation/foundation.tabs
# require base_kzn/foundation/foundation.toggler
# require base_kzn/foundation/foundation.tooltip
# require base_kzn/foundation/foundation.util.box
# require base_kzn/foundation/foundation.util.keyboard
#= require base_kzn/foundation/foundation.util.mediaQuery
# require base_kzn/foundation/foundation.util.motion
# require base_kzn/foundation/foundation.util.nest
# require base_kzn/foundation/foundation.util.timerAndImageLoader
# require base_kzn/foundation/foundation.util.touch
# require base_kzn/foundation/foundation.util.triggers

# Widgets
#
# require widgets/

$ ->
  $(document).foundation()
  svg4everybody()
