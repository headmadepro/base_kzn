# Base Kzn
Гем для базовых ассетов kzn.ru и его подмодулей

## Changelog
[Смотреть тут](https://bitbucket.org/headmadepro/base_kzn/src/b6ac6d6b90271ac96e073f7a53eb940531ec5cb6/CHANGES.md?at=master&fileviewer=file-view-default)

## Установка
В gemfile добавить:
```
#!ruby
gem 'base_kzn', git: 'git@bitbucket.org:headmadepro/base_kzn.git'

```

Запустить команды:
```
#!ruby
bundle

```

```
#!ruby
rails generate base_kzn:install

```

## Обновление гема
```
#!ruby
bundle update --source base_kzn
```

## Работа с SVG

### Метод для вставки иконки из спрайта:
`= svg_icon '<имя иконки из svg спрайта (без префикса icon)>'`

### Доступные модификаторы:
`size: 4` (по умолчанию 4)  
`color: 'alert'` (будет сгенерирован класс 'icon-color-alert')

### Иконка заглушка - default:  
`= svg_icon 'default'`

### Для работы svg иконок в IE подключается библиотека svg4everybody


Миграции на определенную версию см. в файле CHANGES.md