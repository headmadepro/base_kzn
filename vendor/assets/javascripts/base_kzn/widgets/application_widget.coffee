@Widgets ||= {}

class @Widgets.ApplicationWidget

  constructor: (container, options) ->
    @$container = $ container
    options ||= {}
    @options = @_options options
    @initialize()

  initialize: =>
    console.log @options

  destroy: =>
    @_prepare()
    @_unbind()
    @$container.remove()
    delete @

  _prepare: =>

  _unbind: =>

  _default_options: {}

  _options: (options) =>
    options = $.extend {}, @_default_options, options